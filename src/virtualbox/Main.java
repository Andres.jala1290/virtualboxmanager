package virtualbox;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		VBoxManager vbox = new VBoxManager();
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter a command.");
		String option = input.nextLine();
		String machineName;
		while(!option.equals("exit")) {
			switch(option) {
			case "help":
				System.out.println("- machines - Prints a list of the machines with some of its specifications.\n"
						+ "- start - Attempts to start a machine.\n"
						+ "- log - Prints the log of a machine.\n"
						+ "- create - Attempts to create a machine.\n"
						+ "- delete - Attempts to remove a machine and all its files.\n"
						+ "- exit - Ends the program.");
				break;
			case "test":
				System.out.println("Machine name:");
				machineName = input.nextLine();
				vbox.readLog(machineName);
				vbox.startMachine(machineName);
				break;
			case "machines":
				vbox.getMachines();
				break;
			case "log":
				System.out.println("Machine name:");
				machineName = input.nextLine();
				vbox.readLog(machineName);
				break;
			case "create":
				System.out.println("New machine name:");
				machineName = input.nextLine();
				vbox.createMachine(machineName);
				break;
			case "start":
				System.out.println("Machine name:");
				machineName = input.nextLine();
				vbox.startMachine(machineName);
				break;
			case "delete":
				System.out.println("Machine name:");
				machineName = input.nextLine();
				vbox.removeMachine(machineName);
				break;
			default:
				System.out.println("Unknown command.");
			}
			
			System.out.println("Enter a command.");
			option = input.nextLine();
		}
		
		input.close();
	}
}
