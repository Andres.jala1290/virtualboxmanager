package virtualbox;

import org.virtualbox_6_1.*;
import java.util.ArrayList;
import java.util.List;

public class VBoxManager {
	private final VirtualBoxManager boxManager;
	private final IVirtualBox vbox;
    private final String  url = "http://localhost:18083/";
    private final String  user = null;
    private final String  passwd = null;
    
	public VBoxManager() {
		boxManager = VirtualBoxManager.createInstance(null);
		boxManager.connect(url, user, passwd);
		vbox = boxManager.getVBox();
	}
	
	public void cleanup() {
		boxManager.disconnect();
		VirtualBoxManager.deinitPerThread();
	}
	
	public String getVersion() {
		return vbox.getVersion();
	}
	
    public void getMachines()
    {
        List<IMachine> machines = vbox.getMachines();
        for (IMachine m : machines)
        {
            String name;
            Long ram = 0L;
            boolean hwvirtEnabled = false, hwvirtNestedPaging = false;
            boolean paeEnabled = false;
            boolean inaccessible = false;
            try
            {
                name = m.getName();
                ram = m.getMemorySize();
                hwvirtEnabled = m.getHWVirtExProperty(HWVirtExPropertyType.Enabled);
                hwvirtNestedPaging = m.getHWVirtExProperty(HWVirtExPropertyType.NestedPaging);
                paeEnabled = m.getCPUProperty(CPUPropertyType.PAE);
                //String osType = m.getOSTypeId();
                //IGuestOSType foo = vbox.getGuestOSType(osType);
            }
            catch (VBoxException e)
            {
                name = "<inaccessible>";
                inaccessible = true;
            }
            System.out.println("VM name: " + name);
            if (!inaccessible)
            {
                System.out.println(" RAM size: " + ram + "MB"
                                   + ", HWVirt: " + hwvirtEnabled
                                   + ", Nested Paging: " + hwvirtNestedPaging
                                   + ", PAE: " + paeEnabled);
            }
        }
        // process system event queue
        boxManager.waitForEvents(0);
    }
	
    public boolean progressBar(IProgress p, long waitMillis)
    {
        long end = System.currentTimeMillis() + waitMillis;
        while (!p.getCompleted())
        {
            // process system event queue
            boxManager.waitForEvents(0);
            // wait for completion of the task, but at most 200 msecs
            p.waitForCompletion(200);
            if (System.currentTimeMillis() >= end) {
                return false;
            }
        }
        return true;
    }
    
    public void readLog(String machineName)
    {
        IMachine m =  vbox.findMachine(machineName);
        long logNo = 0;
        long off = 0;
        long size = 16 * 1024;
        while (true)
        {
            byte[] buf = m.readLog(logNo, off, size);
            if (buf.length == 0)
                break;
            System.out.print(new String(buf));
            off += buf.length;
        }
        // process system event queue
        boxManager.waitForEvents(0);
    }
	
	public void startMachine(String machineName) {
		IMachine m = vbox.findMachine(machineName);
		System.out.println("\nAttempting to start VM '" + machineName + "'");
		
		ISession session = boxManager.getSessionObject();
		ArrayList<String> env = new ArrayList<String>();
		IProgress p = m.launchVMProcess(session, "headless", env);
		progressBar(p, 10000);
		session.unlockMachine();
		boxManager.waitForEvents(0);
	}
    
	public void createMachine(String name) {
		String settingsFile = "";
		String osTypeId = "Ubuntu_64";
		String flags = "";
		
		vbox.createMachine(settingsFile, name, null, osTypeId, flags);
	}

    
    public void removeMachine(String machineName) {
    	IMachine m = vbox.findMachine(machineName);
    	m.unregister(CleanupMode.Full);
    }
}
