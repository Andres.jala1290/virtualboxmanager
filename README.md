# VirtualBoxManager

Implementation of the VirtualBox SDK to manage virtual machines.

## Initial Setup

- Add the VirtualBox bin folder to the PATH Environment Variable of the machine.
- Add the VirtualBox WebService library for java "vboxjws.jar" located inside the VirtualBox SDK folder "sdk\bindings\webservice\java\jax-ws" to the class path libraries of the Java project. (The "vboxjws.jar" will also be on the libs folder inside this project.)
- To run the web service, open a terminal and run the command "vboxwebsrv -v", the -v causes to log all connections to the terminal.

## Commands

When the code is executed, it will ask for a command, here's a list of the available commands:

- help - Lists the available commands.
- machines - Prints a list of the machines with some of its specifications.
- start - Attempts to start a machine.
- log - Prints the log of a machine.
- create - Attempts to create a machine.
- delete - Attempts to remove a machine and all its files.
- exit - Ends the program.

## Methods

There are the methods on the VBoxManager class:

- getVersion() : String - Returns the version of VirtualBox installed on the machine.
- getMachines() : void - Prints a list of the machines with some of its specifications.
- startMachine() : void - Attempts to run a machine.
- readLog() : void - Prints the log of a machine.
- createMachine(machineName : String) : void - Attempts to create a machine.
- removeMachine(machineName : String) : void - Attempts to remove a machine and all its files.



